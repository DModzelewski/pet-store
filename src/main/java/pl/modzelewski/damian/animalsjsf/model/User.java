package pl.modzelewski.damian.animalsjsf.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 *
 * @author Damian
 */
public class User implements Serializable{
    
    private String login;
    private String password;
    private String name;
    private Gender gender;
    private List<Animal> ownedAnimals;
    private BigDecimal money;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public List<Animal> getOwnedAnimals() {
        return ownedAnimals;
    }

    public void setOwnedAnimals(List<Animal> ownedAnimals) {
        this.ownedAnimals = ownedAnimals;
    }

    public BigDecimal getMoney() {
        money = money.setScale(2, RoundingMode.CEILING);
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public User() {
    }

    public User(BigDecimal money) {
        this.money = money;
    }
    
    
    
}
