/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.modzelewski.damian.animalsjsf.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author Damian
 */
public class Animal implements Serializable{
    private int id;
    private String name;
    private String species;
    private double weight;
    private BigDecimal price;
    private String imgPath = "img/default.jpg";
    private String description = "No description has been added";

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public BigDecimal getPrice() {
        price = price.setScale(2, RoundingMode.CEILING);
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public Animal(int id, String name, String species, double weight, BigDecimal price) {
        this.id = id;
        this.name = name;
        this.species = species;
        this.weight = weight;
        this.price = price;
    }

    public Animal(int id, String name, String species, double weight, BigDecimal price, String imgPath) {
        this.id = id;
        this.name = name;
        this.species = species;
        this.weight = weight;
        this.price = price;
        this.imgPath = imgPath;
    }

    public Animal() {
    }
    
    
    
    
}
