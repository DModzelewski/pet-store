/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.modzelewski.damian.animalsjsf.model;

/**
 *
 * @author Damian
 */
public enum Gender {
    MALE,
        FEMALE,
        BIGENDER,
        ANDROGYNE,
        NEUTRORIS,
        INTERGENDER,
        DEMIBOY,
        DEMIGIRL,
        GENDERQUEEN,
        POLIGENDER,
        EPICENE,
        GENDERFLUID,
        TRANSGENDER,
        AGENDER,
        TRANSVESTI,
        BUTCH,
        ALIAGENDER,
        AESTHETIGENDER,
        AMBIGENDER,
        ANGENIAL,
        ASTRALGENDER,
        ASTERGENDER,
        AUTOGENDER,
        BIOGENDER,
        CASSGENDER,
        CISGENDER,
        CLOUDGENDER,
        COLORGENDER,
        DEMIFLUX,
        DELICIAGENDER,
        EXGENDER,
        GENDERBLANK,
        GLASSGENDER,
        GENDERPUNK,
        HELIOGENDER,
        JUXERA,
        IMPERIGENDER,
        HYDROGENDER,
        SUBGENDER,
        VELOCIGENDER
}
