package pl.modzelewski.damian.animalsjsf.databases;

import java.math.BigDecimal;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import pl.modzelewski.damian.animalsjsf.model.*;

/**
 *
 * @author Damian
 */
@ApplicationScoped
public class UserDatabase {

    /**
     * collection of animals in the store
     */
    private User user = new User();

    @PostConstruct
    public void init() {

        user.setName("Ula");
        user.setLogin("admin");
        user.setPassword("4dm1n");
        user.setMoney(new BigDecimal(1000.25));
        user.setGender(Gender.FEMALE);
        user.setOwnedAnimals(new ArrayList<>());
        
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
       
}
