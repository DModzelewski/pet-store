package pl.modzelewski.damian.animalsjsf.databases;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import pl.modzelewski.damian.animalsjsf.model.*;

/**
 *
 * @author Damian
 */
@ApplicationScoped
public class AnimalDatabase {

    /**
     * collection of animals in the store
     */
    private List<Animal> animals = new ArrayList<>();

    @PostConstruct
    public void init() {

        Animal a1 = new Animal(1, "Dog", "Mammal", 15, new BigDecimal(150), "img/dog.jpg");
        Animal a2 = new Animal(2, "Cat", "Mammal", 5,  new BigDecimal(150), "img/cat.jpg");
        Animal a3 = new Animal(3, "Gecko", "Reptile", 0.8,  new BigDecimal(250.99), "img/gecko.jpg");
        Animal a4 = new Animal(4, "Fly", "Insect", 0.01,  new BigDecimal(0.10), "img/fly.jpg");
        Animal a5 = new Animal(5, "Snake", "Reptile", 1,  new BigDecimal(120), "img/snek.jpg");
        Animal a6 = new Animal(6, "Crow", "Bird", 0.5,  new BigDecimal(100), "img/crow.jpg");

        a1.setDescription("a domesticated carnivorous mammal that typically has a long snout, an acute sense of smell, non-retractile claws, and a barking, howling, or whining voice.");
        
        animals.addAll(Arrays.asList(a1, a2, a3, a4, a5, a6));
    }

    public List<Animal> getAnimals() {
        return animals;
    }

    public Animal getAnimal(int id) {
        return animals.get(id);
    }

}
