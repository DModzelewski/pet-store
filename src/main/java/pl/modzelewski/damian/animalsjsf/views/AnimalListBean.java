package pl.modzelewski.damian.animalsjsf.views;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import pl.modzelewski.damian.animalsjsf.databases.*;
import pl.modzelewski.damian.animalsjsf.model.*;

/**
 *
 * @author Damian
 */
@ManagedBean
@ViewScoped
public class AnimalListBean implements Serializable {

    @Inject
    private AnimalDatabase animalStore;

    @Inject
    private UserDatabase userStore;

    private List<Animal> animals;
    private User user;

    public List<Animal> getAnimals() {
        animals = null;
        if (animals == null) {
            animals = animalStore.getAnimals();
        }
        return animals;
    }

    public User getUser() {
        if (user == null) {
            user = userStore.getUser();
        }
        return user;
    }

    public void buy(Animal animal) {
        BigDecimal userMoney = userStore.getUser().getMoney();
        BigDecimal animalPrice = animal.getPrice();
        userStore.getUser().setMoney(userMoney.subtract(animalPrice));
        userStore.getUser().getOwnedAnimals().add(animal);
    }

    public boolean isEnoughMoney(Animal animal) {
        BigDecimal userMoney = userStore.getUser().getMoney();
        BigDecimal animalPrice = animal.getPrice();
        int res = userMoney.compareTo(animalPrice);
        if (res >= 1) {
            return true;
        } else {
            return false;
        }
    }

}
