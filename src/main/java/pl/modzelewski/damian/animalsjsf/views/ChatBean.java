/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.modzelewski.damian.animalsjsf.views;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import pl.modzelewski.damian.animalsjsf.databases.UserDatabase;

/**
 *
 * @author Damian
 */
@ManagedBean
@ApplicationScoped
public class ChatBean {
    
    @Inject
    UserDatabase user;
    
    private List<String> chatLog = new ArrayList<>();
    private String chat;
    private String msg;

    public List<String> getChatLog() {
        return chatLog;
    }

    public String getChat() {
        chat = chatLog.stream().collect(Collectors.joining());
        return chat;
    }

    public void setChat(String chat) {
        this.chat = chat;
    }
    
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    
    public void send(){
        if(msg!=null){
            chatLog.add(user.getUser().getLogin()+": "+msg+"\n");
            msg=null;
        }
    }
    
}
