package pl.modzelewski.damian.animalsjsf.views;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import pl.modzelewski.damian.animalsjsf.databases.AnimalDatabase;
import pl.modzelewski.damian.animalsjsf.model.*;

/**
 *
 * @author Damian
 */
@ManagedBean
@Named
@ViewScoped
public class AnimalViewBean implements Serializable{

    @Inject
    private AnimalDatabase animalStore;
    
    private int animalId;
    private Animal animal;

    public int getAnimalId() {
        return animalId;
    }

    public void setAnimalId(int animalId) {
        this.animalId = animalId;
    }

    public Animal getAnimal() {
        if(animal==null){
            animal = animalStore.getAnimal(animalId-1);
        }
        return animal;
    } 
    
}
