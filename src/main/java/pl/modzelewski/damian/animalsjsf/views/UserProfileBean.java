package pl.modzelewski.damian.animalsjsf.views;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import pl.modzelewski.damian.animalsjsf.databases.UserDatabase;
import pl.modzelewski.damian.animalsjsf.model.*;

/**
 *
 * @author Damian
 */
@Named
@ViewScoped
public class UserProfileBean implements Serializable {

    @Inject
    private UserDatabase userDB;

    private User user;
    private String newName;
    private Gender[] genders = Gender.values();
    private boolean showInput = false;
    private boolean showGenders = false;

    public User getUser() {
        if (user == null) {
            user = userDB.getUser();
        }
        return user;
    }
    
    public String getNewName() {
        return newName;
    }

    public void setNewName(String newName) {
        this.newName = newName;
    }

    public Gender[] getGenders() {
        return genders;
    }

    public void setGenders(Gender[] genders) {
        this.genders = genders;
    }
    
    public boolean isShowGenders() {
        return showGenders;
    }

    public void setShowGenders(boolean showGenders) {
        this.showGenders = showGenders;
    }

    public boolean isShowInput() {
        return showInput;
    }

    public void setShowInput(boolean showInput) {
        this.showInput = showInput;
    }

    public boolean isListPopulated() {
        return !user.getOwnedAnimals().isEmpty();
    }

    public void changeName(){
        user.setName(newName);
        newName=null;
    }
}
