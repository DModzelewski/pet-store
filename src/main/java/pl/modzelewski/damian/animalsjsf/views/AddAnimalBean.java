package pl.modzelewski.damian.animalsjsf.views;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Part;
import org.apache.commons.io.IOUtils;
import pl.modzelewski.damian.animalsjsf.databases.AnimalDatabase;
import pl.modzelewski.damian.animalsjsf.model.*;

/**
 *
 * @author Damian
 */
@ManagedBean
@Named
@ViewScoped
public class AddAnimalBean implements Serializable {

    @Inject
    private AnimalDatabase animalStore;

    private String name;
    private String species;
    private double weight;
    private BigDecimal price;
    private String imgPath;
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void addAnimal() {
        Animal newAnimal = new Animal();
        newAnimal.setName(name);
        newAnimal.setSpecies(species);
        newAnimal.setWeight(weight);
        newAnimal.setPrice(price);
        newAnimal.setDescription(description);
        newAnimal.setId(animalStore.getAnimals().size() + 1);
        animalStore.getAnimals().add(newAnimal);
    }

}
